package jp.co.us717.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;

import org.apache.commons.lang3.StringUtils;

/**
 * 見出し。<br>
 * データベースから、スレッド一覧と指定されたページに該当するスレッドの最新のレスを取得します。<br>
 */
public class HeadLineBean extends BeanCommonFunction {
    /** スレッド一覧リスト */
    private ArrayList<HashMap<String, String>> threadTableList_;

    /** 最新のレスリスト */
    private ArrayList<HashMap<String, String>> newlyArrivedResList_;

    /**
     * コンストラクタ。
     */
    public HeadLineBean() {
        threadTableList_ = new ArrayList<>();
        newlyArrivedResList_ = new ArrayList<>();
    }

    /**
     * データベースから、見出し取得<br>
     * スレッドの一覧と、指定されたページに該当するスレッドの最新のレスを取得します。<br>
     * スレッドは最終更新の順で取得されます。
     * @param numberOfThreadTable    表示するスレッド一覧の件数
     * @param numberOfNewRes         最新のレス扱いとなる件数
     * @param numberOfArticle        1ページに表示する記事数
     * @param pageNumber             表示したいページ番号(0ページから始まります)
     * @throws SQLRuntimeException   データベースでのエラーが発生した場合
     */
    public void SelectHeadLine(int numberOfThreadTable, int numberOfNewRes, int numberOfArticle, int pageNumber) {

        //====初期化・定義================
        threadTableList_.clear();
        newlyArrivedResList_.clear();
        int articleBegin = numberOfArticle * pageNumber; //記事通し番号(ページはじめ)
        int articleEnd = numberOfArticle * (pageNumber + 1);//記事通し番号(ページ終わり)
        ArrayList<Integer> tmpTargetThreadIdList = new ArrayList<Integer>();//表示対象となるスレッドのID
        ArrayList<Integer> tmpTargetResCountList = new ArrayList<Integer>(); //表示対象となるスレッドの最新レス数格納用

        //====スレッド一覧読み込み==========
        String sqlSelectThreadTable = "select thread_id, thread_name, last_update, created_date, res_count "
                + "from thread where state=true order by last_update desc limit ?";

        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmGetDeleteFileName = con.prepareStatement(sqlSelectThreadTable)) {

            //データ取得
            stmGetDeleteFileName.setInt(1, numberOfThreadTable);
            ResultSet rs = stmGetDeleteFileName.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();

            //データ格納
            int rowCount = 0; //取得行数
            while (rs.next()) {
                rowCount++;

                //各カラムを格納
                threadTableList_.add(StoreColumn(rs, rsmd));

                //表示対象となるレス用のデータを別に格納
                if (rowCount > articleBegin && rowCount <= articleEnd) {
                    tmpTargetThreadIdList.add(rs.getInt(1));
                    tmpTargetResCountList.add(rs.getInt(5));
                }
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }

        //====新着レス読み込み==============

        //処理を継続するかチェック
        if (tmpTargetThreadIdList.size() == 0) {
            return; //表示対象となるスレッドが存在しなければこれ以上処理しない
        }

        //SQL作成(表示対象となるスレッド分in句のパラメータを作成)
        StringBuilder sqlSelectNewRes = new StringBuilder();
        sqlSelectNewRes
                .append("select *from(select thread_id, poster, postdate, is_password, password, filename, state, "
                        + "row_number() over(partition by thread_id order by postdate desc )num from res)result "
                        + "where num<= ? and thread_id in(");
        sqlSelectNewRes.append(StringUtils.join(Collections.nCopies(tmpTargetThreadIdList.size(), "?").toArray(), ","));
        sqlSelectNewRes.append(")");

        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmSelectNewRes = con.prepareStatement(sqlSelectNewRes.toString())) {

            //データ取得
            stmSelectNewRes.setInt(1, numberOfNewRes);
            for (int i = 0; i < tmpTargetThreadIdList.size(); i++) {
                stmSelectNewRes.setInt(i + 2, tmpTargetThreadIdList.get(i));
            }
            ResultSet rs = stmSelectNewRes.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();

            //データ格納
            Iterator<Integer> ite = tmpTargetResCountList.iterator();
            Deque<HashMap<String, String>> stack = new ArrayDeque<>();
            while (ite.hasNext()) {
                int tmpResCount = ite.next();

                //スタック格納
                if (tmpResCount != 0) {
                    int i = 0;
                    while (rs.next() && i < tmpResCount) {
                        stack.addLast(StoreColumn(rs, rsmd));
                    }
                }

                //データ挿入
                while (!stack.isEmpty()) {
                    newlyArrivedResList_.add(stack.pollLast());
                }
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * スレッド一覧リスト取得。
     * @return ArrayList スレッド一覧リスト
     */
    public ArrayList<HashMap<String, String>> getThreadTableList() {
        return threadTableList_;
    }

    /**
     * 最新のレスリスト取得。
     * @return ArrayList 最新のレスリスト
     */
    public ArrayList<HashMap<String, String>> getNewlyArrivedResList() {
        return newlyArrivedResList_;
    }

    /**
     * カラム格納
     * @param resultSet
     * @param resultSetMetaData
     * @return
     * @throws SQLException
     */
    private HashMap<String, String> StoreColumn(ResultSet resultSet, ResultSetMetaData resultSetMetaData)
            throws SQLException {
        HashMap<String, String> record = new HashMap<>();
        for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
            String columnName = resultSetMetaData.getColumnName(i);
            String date = resultSet.getString(i);
            record.put(columnName, date);
        }
        return record;
    }
}
