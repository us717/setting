package jp.co.us717.model;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.func.FuncDBSetup;
import static org.hamcrest.CoreMatchers.*;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * PostResBeanのテスト。
 * thread_id(1)をテスト用のレコードとして使用している。
 * テストデータあり
 */

public class PostResBeanTest {

    /** データベース元データ保存用ファイル */
    private File file;

    /** データベース接続用情報  */
    private FuncDBSetup dbSet;

    /**
     * JNDIのセットアップ
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncDBSetup.SetJNDI();
    }

    /**
     * バックアップ作成。
     * @throws Exception エラー全般
     */
    @Before
    public void Before() throws Exception {

        //DataSourse作成
        dbSet = new FuncDBSetup();
        dbSet.loadSetting();

        IDatabaseConnection IDBcon = null;
        try (Connection con = dbSet.getDataSource().getConnection();
                Statement stm = con.createStatement()) {
            IDBcon = new DatabaseConnection(con);

            //DBバックアップ
            QueryDataSet partialDataSet = new QueryDataSet(IDBcon);
            partialDataSet.addTable("thread");
            partialDataSet.addTable("res");
            file = File.createTempFile("tmp", ".xml");
            FlatXmlDataSet.write(partialDataSet, new FileOutputStream(file));

            //テストデータ挿入
            IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(
                    "src/test/resources/sqlTestData/PostResBeanTest.xml"));
            DatabaseOperation.CLEAN_INSERT.execute(IDBcon, dataSet);
        } finally {
            if (IDBcon != null)
                IDBcon.close();
        }
    }

    /**
     * データベース復元。
     * @throws Exception エラー全般
     */
    @AfterClass
    public void tearDown() throws Exception {
        IDatabaseConnection IDBcon = null;
        try (Connection con = dbSet.getDataSource().getConnection();) {
            IDBcon = new DatabaseConnection(con);
            //データベース復元
            IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(file));
            DatabaseOperation.CLEAN_INSERT.execute(IDBcon, dataSet);
        } finally {
            if (IDBcon != null)
                IDBcon.close();
        }
    }

    /**
     * データベース削除。
     * @throws Exception エラー全般
     */
    @After

    /**
     * 正常なデータを渡すと投稿は成功する。<br>
     * thread_id(1)のレスが一件登録される。<br>
     * 画像データも保存されている(目視確認)
     * @throws Exception エラー全般
     */
    @Test
    public void InsertRes_正常なレス投稿() throws Exception {
        PostResBean postres = new PostResBean();

        //テスト用画像データ準備
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                "src/test/resources/testPaintDate.txt")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();

        //テスト
        postres.setDBConnect(dbSet.getDataSource());
        postres.setImageData(sb.toString());
        postres.setThreadNum(1);
        postres.setSaveLocation("src/test/resources/testimage");
        postres.InsertRes();

        //チェック
        try (Connection con = dbSet.getDataSource().getConnection();
                Statement stm = con.createStatement();) {
            ResultSet result = stm.executeQuery("select *from res");

            int resCount = 0;
            while (result.next()) {
                assertThat(result.getString("thread_id"), is("1"));
                assertThat(result.getString("poster"), is("名無し"));
                assertThat(result.getString("state"), is("t"));
                resCount++;
            }
            assertThat(resCount, is(1));
        }
    }

    /**
     * スレが書き込み禁止だった場合、例外(SQLRuntimeException)が発生する。
     */
    @Test(expected = SQLRuntimeException.class)
    public void InsertRes_スレが投稿禁止() throws Exception {
        PostResBean postres = new PostResBean();

        //テスト用画像データ準備
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                "src/test/resources/testPaintDate.txt")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();

        //テスト
        postres.setDBConnect(dbSet.getDataSource());
        postres.setImageData(sb.toString());
        postres.setThreadNum(2);
        postres.setSaveLocation("src/test/resources/testimage");
        postres.InsertRes();
    }

    /**
     * 違反データでデータベースに挿入できなかった場合、例外(SQLExcwption)が発生する。<br>
     * スレッド番号を設定していない。
    */
    @Test(expected = SQLRuntimeException.class)
    public void InsertRes_異常なDB用データ() throws Exception {
        PostResBean postres = new PostResBean();

        //テスト用画像データの準備
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                "src/test/resources/testPaintDate.txt")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();

        //テスト(スレ番号を設定していない)
        postres.setDBConnect(dbSet.getDataSource());
        postres.setSaveLocation("src/test/resources/testimage");
        postres.setImageData(sb.toString());
        postres.InsertRes();
    }

    /*
     * NULLを渡すと例外(NullPointerException)が発生する。
     */
    @Test(expected = NullPointerException.class)
    public void setDBConnect_Nullを挿入() {
        PostResBean postres = new PostResBean();
        postres.setDBConnect(null);
    }

    /**
     * 正常値を渡すと結果はフィールドに反映される。
     */
    @Test
    public void setPoster_正常な挿入() throws Exception {
        //テスト設定
        PostResBean postres = new PostResBean();
        postres.setPoster("投稿者");

        //privateなフィールドにアクセスできるようにする。
        Field field = postres.getClass().getDeclaredField("poster");
        field.setAccessible(true);
        String result = "投稿者";

        //チェック
        assertThat(field.get(postres), is(result));
    }

    /**
     * NULLの場合、結果は反映されない。
     */
    @Test
    public void setPoster_Nullの挿入() throws Exception {
        //テスト設定
        PostResBean postres = new PostResBean();
        postres.setPoster(null);

        //privateなフィールドにアクセスできるようにする。
        Field field = postres.getClass().getDeclaredField("poster");
        field.setAccessible(true);
        String result = "名無し";

        //チェック
        assertThat(field.get(postres), is(result));
    }

    /**
     * 空文字の場合、結果は反映されない。
     */
    @Test
    public void setPoster_空文字の挿入() throws Exception {
        //テスト設定
        PostResBean postres = new PostResBean();
        String data = new String("");
        postres.setPoster(data);

        //privateなフィールドにアクセスできるようにする。
        Field field = postres.getClass().getDeclaredField("poster");
        field.setAccessible(true);
        String result = "名無し";

        //チェック
        assertThat(field.get(postres), is(result));
    }

    /**
     * スペースの場合、結果は反映されない。
     */
    @Test
    public void setPoster_スペースの挿入() throws Exception, IllegalAccessException {
        //テスト設定
        PostResBean postres = new PostResBean();
        String data = new String("   ");
        postres.setPoster(data);

        //privateなフィールドにアクセスできるようにする。
        Field field = postres.getClass().getDeclaredField("poster");
        field.setAccessible(true);
        String result = "名無し";

        //チェック
        assertThat(field.get(postres), is(result));
    }

    /**
     * 半角英数を挿入するとフィールドに反映される。
     */
    @Test
    public void setPassword_正常な挿入() throws Exception {
        //テスト設定
        PostResBean postres = new PostResBean();
        postres.setPassword("Test1234");

        //privateなフィールドにアクセスできるようにする。
        Field password = postres.getClass().getDeclaredField("password");
        Field isPassword = postres.getClass().getDeclaredField("isPassword");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(postres), is("Test1234"));
        assertThat(isPassword.get(postres), is(true));
    }

    /**
     * 全角が含まれていると反映されない
     */
    @Test
    public void setPassword_全角が含まれている挿入() throws Exception {
        //テスト設定
        PostResBean postres = new PostResBean();
        postres.setPassword("test１２３４");

        //privateなフィールドにアクセスできるようにする。
        Field password = postres.getClass().getDeclaredField("password");
        Field isPassword = postres.getClass().getDeclaredField("isPassword");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(postres), is("0000"));
        assertThat(isPassword.get(postres), is(false));
    }

    /**
     * NULLの場合反映されない
     */
    @Test
    public void setPassword_NULLの場合() throws Exception {
        //テスト設定
        PostResBean postres = new PostResBean();
        postres.setPassword(null);

        //privateなフィールドにアクセスできるようにする。
        Field password = postres.getClass().getDeclaredField("password");
        Field isPassword = postres.getClass().getDeclaredField("isPassword");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(postres), is("0000"));
        assertThat(isPassword.get(postres), is(false));
    }

    /**
     * 空文字の場合反映されない。
     */
    @Test
    public void setPassword_空文字の場合() throws Exception {
        //テスト設定
        PostResBean postres = new PostResBean();
        postres.setPassword("");

        //privateなフィールドにアクセスできるようにする。
        Field password = postres.getClass().getDeclaredField("password");
        Field isPassword = postres.getClass().getDeclaredField("isPassword");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(postres), is("0000"));
        assertThat(isPassword.get(postres), is(false));
    }

    /**
     * スペースの場合反映されない。
     */
    public void setPassword_スペースの場合() throws Exception {

        //テスト設定
        PostResBean postres = new PostResBean();
        postres.setPassword(" ");

        //privateなフィールドにアクセスできるようにする。
        Field password = postres.getClass().getDeclaredField("password");
        Field isPassword = postres.getClass().getDeclaredField("isPassword");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(postres), is("9999"));
        assertThat(isPassword.get(postres), is(false));
    }

}
