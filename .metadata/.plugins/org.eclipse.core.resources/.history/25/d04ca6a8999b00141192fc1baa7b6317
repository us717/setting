package jp.co.us717.func;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * private指定にアクセスするための機能。<br>
 * テスト時、外部アクセスを制限しているメソッド、フィールドへのアクセス手段を提供します。
 */
public class FuncPrivateAccessor {

    /**
     * 外部アクセスが禁止されているフィールドの取得。
     * @param    targetField 変更するフィールドを所持するクラス
     * @param    filedName   変更するフィールド名
     * @return   filed       外部アクセスが禁止されたフィールド
     * @throws RuntimeException エラー全般
     */
    static Object getPrivateField(Object targetClass, String filedName) {
        Field filed = null;
        try {
            filed = targetClass.getClass().getDeclaredField(filedName);
        } catch (NoSuchFieldException | SecurityException e) {
            throw new RuntimeException(e);
        }
        filed.setAccessible(true);
        return filed;
    }

    /**
     * 外部アクセスが禁止されているフィールドに値を設定します。
     * @param    targetField    変更するフィールドを所持するクラス
     * @param    filedName      変更するフィールド名
     * @return   filed          アクセスが許可されたフィールド
     * @throws RuntimeException エラー全般
     */
    static void SetPrivateField(Object targetClass, String filedName, Object value ) {
        Field filed = null;
        try {
            filed = targetClass.getClass().getDeclaredField(filedName);
            filed.setAccessible(true);
            filed.set(targetClass, value);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    static void doPrivateMethod(Object targetClass, String methodName, Class<?>[] dataTypeArray, Object[] paramArray ) {
        try {
            Method method = targetClass.getClass().getDeclaredMethod(methodName, dataTypeArray);
            method.setAccessible(true);
        } catch (NoSuchMethodException | SecurityException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
    }
}
