package jp.co.us717.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.us717.exception.GeneralErrorException;
import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;

import org.apache.commons.lang3.StringUtils;

/**
 * スレッドの作成。
 */
public class CreateThreadBean extends BeanCommonFunction {

    /** スレッド名 */
    private String threadName_;

    /** 描画枠:横幅 */
    private int width_;

    /** 描画枠:高さ */
    private int height_;

    /**
     * コンストラクタ
     */
    CreateThreadBean() {
    }

    /**
     * スレッドの作成。<br>
     * 新しいスレッドを作成します。<br>
     * @throws SQLRuntimeException スレッドの作成が失敗した場合。
     */
    public void CreateThread() {

        String sqlCreateThread = "insert into thread(thread_name, width, height, last_update, created_date)"
                + "values(?,?,?,now(),now())";
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmCreateThread = con.prepareStatement(sqlCreateThread)) {
            stmCreateThread.setString(1, threadName_);
            stmCreateThread.setInt(2, width_);
            stmCreateThread.setInt(3, height_);
            stmCreateThread.execute();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * スレッド名の設定。
     * 空文字、NULL、スペースのみの設定は許可されていません。
     * @param threadName_ スレッド名
     * @throws GeneralErrorException スレッド名が不正な値
     */
    public void setThreadName(String threadName) {
        if (StringUtils.isBlank(threadName_))
            throw new GeneralErrorException("スレッド名に不正な値を設定しようとしています。");
        threadName_ = threadName.trim();
    }

    /**
     * 横幅の設定。
     * @param width_ 描画枠横幅
     */
    public void setWidth(int width) {
        width_ = width;
    }

    /**
     * 高さの設定。
     * @param height 描画枠高さ
     */
    public void setHeight(int height) {
        height_ = height_;
    }
}
