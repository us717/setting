package jp.co.us717.model;

import jp.co.us717.func.FuncDBSetup;
import jp.co.us717.func.FuncJNDISetup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.util.HashMap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * HeadLineBeanのテスト。<br>
 */
public class HeadLineBeanTest {

    /** JNDI */
    private FuncJNDISetup jndi;

    /**
     * JNDIセットアップ、データバックアップ
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource(), "src/test/resources/sqlTestData/HeadLineBeanTest.xml");
    }

    /**
     * テスト前処理
     */
    @Before
    public void Before(){
        jndi = new FuncJNDISetup();
        jndi.loadSetting();
        FuncDBSetup.Delete(jndi.getDataSource());
    }

    /**
     * データベース復元
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * 正常にスレッド一覧が取得できているか。<br>
     * thread_id(1と10)以外の9から2までが大きい順に取得できる。
     * @throws exception
     */
    @Test
    public void SelectHeadLine_スレッド一覧が正しく取得できるか() throws Exception {

        //テスト
        HeadLineBean headLine = new HeadLineBean();
        headLine.setDBConnect(jndi.getDataSource());
        headLine.SelectHeadLine(10, 3, 3, 0);

        //チェック
        int threadCount = 0;
        String r[] = { "9", "8", "7", "6", "5", "4", "3", "2" };
        for (HashMap<String, String> i : headLine.getThreadTableList()) {
            assertThat(i.get("thread_id"), is(r[threadCount]));
            threadCount++;
        }

        assertThat(threadCount, is(8));
    }

    /**
     * 正常に新着レスが取得できているか。<br>
     * thrad_id2で、filename3,4,5の順で取得できている。
     */
    @Test
    public void SelectHeadLine_新着レスが正しく取得できているか() {
        //テスト
        HeadLineBean headLine = new HeadLineBean();
        headLine.setDBConnect(jndi.getDataSource());
        headLine.SelectHeadLine(10, 3, 5, 1);

        //チェック
        int resCount = 0;
        String r[] = { "3.jpg", "4.jpg", "5.jpg" };

        for (HashMap<String, String> i : headLine.getNewResList()) {
            assertThat(i.get("thread_id"), is("2"));
            assertThat(i.get("filename"), is(r[resCount]));
            resCount++;
        }

        assertThat(resCount, is(3));
    }

}
