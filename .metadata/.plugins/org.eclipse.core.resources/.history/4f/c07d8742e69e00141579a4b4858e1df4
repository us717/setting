package jp.co.us717.model;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

import jp.co.us717.exception.GeneralErrorException;
import jp.co.us717.exception.IORuntimeException;
import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;

import org.apache.commons.lang3.StringUtils;

/**
 * レス投稿。<br>
 */
public class PostResBean extends BeanCommonFunction {

    /** 投稿するスレ番号 */
    private int threadNumber;

    /** 投稿者の名前 */
    private String poster;

    /** ファイル名 */
    private String filename;

    /** パスワード */
    private String password;

    /** パスワードの有無*/
    private boolean isPassword;

    /** 画像データ */
    private String postImage;

    /**
     * コンストラクタ
     */
    public PostResBean() {
        poster = "名無し";
        filename = "";
        password = "0000";
        postImage = "";
    }

    /**
     * レスを投稿する。
     * @throws SQLRuntimeException レスの投稿に失敗した場合
     * @throws IORuntimeException  画像データの保存に失敗した場合
     */
    public void InsertRes() {

        //ファイル名を作成する（現時刻+乱数)
        Date curDate = new Date();
        DateFormat reformDate = new SimpleDateFormat("yyMMddHHmmss");
        Random rand = new Random();
        this.filename = reformDate.format(curDate) + rand.nextInt(999) + ".jpg";

        //画像データをデコードしてディレクトリに保存
        File file = new File(this.realPath + "/" + this.filename);
        try (FileChannel fc = FileChannel.open(file.toPath(), StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
                FileLock lock = fc.tryLock()) {
            if (lock == null) {
                throw new GeneralErrorException();
            }
            byte[] encodedData = Base64.getDecoder().decode(this.postImage);
            ByteBuffer buf = ByteBuffer.wrap(encodedData);
            fc.write(buf);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }

        //SQL定義
        //====レスの新規作成========
        String sqlRes = "insert into res(thread_id, poster, filename, postdate, is_password, password )"
                + "values(?, ?, ?, now(), ?, ?)";
        //====スレッドの最終更新日更新,レス数 ======
        String sqlThread = "update thread set last_update = now(), res_count = res_count + 1 where thread_id = ? and state = true";

        //DB更新
        try (Connection con = this.tmpDataSource.getConnection();
                PreparedStatement stmRes = con.prepareStatement(sqlRes);
                PreparedStatement stmThread = con.prepareStatement(sqlThread)) {
            try {
                //トランザクション開始
                con.setAutoCommit(false);

                //レス新規作成
                stmRes.setInt(1, this.threadNumber);
                stmRes.setString(2, this.poster);
                stmRes.setString(3, this.filename);
                stmRes.setBoolean(4, this.isPassword);
                stmRes.setString(5, this.password);
                stmRes.executeUpdate();

                //threadの最終更新日、レス数を更新する
                stmThread.setInt(1, this.threadNumber);
                if (stmThread.executeUpdate() != 1) { //スレッドが書き込み禁止だった場合中止
                    throw new SQLException("ERROR:スレッドが書き込み禁止状態です。スレッド" + this.threadNumber);
                }

                //コミット
                con.commit();

            } catch (SQLException ex) {
                con.rollback();
                throw ex;
            } finally {
                con.setAutoCommit(true);
            }
        } catch (SQLException e) {
            //削除
            try {
                Files.deleteIfExists(file.toPath());//失敗したので、既に保存していた画像を削除する
            } catch (IOException ex) {
                throw new IORuntimeException(ex);
            }
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * スレ番号の設定。
     * @param threadNumber 投稿するDB内のスレ番号
     */
    public void setThreadNum(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    /**
     * 投稿者の名前設定。<br>
     * 任意。<br>
     * 空文字、NULL、スペースを設定した場合はデフォルトの名前で設定は反映されません。
     * @param poster 投稿者の名前
     */
    public void setPoster(String poster) {
        if (!StringUtils.isBlank(poster)) {
            this.poster = poster.trim();
        } else {
            this.poster = "名無し"; //デフォルトの投稿者名
        }
    }

    /**
     * パスワードの設定。<br>
     * 任意<br>
     * 半角英数字を設定して下さい。<br>
     * 半角英数字以外の場合、パスワードは設定されません。
     * @param password 削除用のパスワード(半角英数字)
     */
    public void setPassword(String password) {
        if (!StringUtils.isBlank(password) && password.matches("^[a-zA-Z0-9]+$")) {
            this.password = password.trim();
            this.isPassword = true;
        } else {
            this.password = "0000";
            this.isPassword = false;
        }
    }

    /**
     * 画像データの設定。
     * @param postImage デコードして保存する画像データ
     */
    public void setImageData(String postImage) {
        this.postImage = Objects.requireNonNull(postImage);
    }
}