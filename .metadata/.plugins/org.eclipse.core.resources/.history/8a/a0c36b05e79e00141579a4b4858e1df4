package jp.co.us717.func;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

/**
 * データベースにテストデータを挿入するためのセットアップ。
 */
public class FuncDBSetup {

    /** データベース本番データ避難用ファイル */
    private static File tmpFile;

    /**
     * バックアップ作成。
     * @param ds データベース接続情報
     * @throws RuntimeException エラー全般
     */
    public static void Backup(DataSource ds) {

        IDatabaseConnection IDBcon = null;
        try (Connection con = ds.getConnection()) {
            IDBcon = new DatabaseConnection(con);

            //DBバックアップ
            QueryDataSet partialDataSet = new QueryDataSet(IDBcon);
            partialDataSet.addTable("thread");
            partialDataSet.addTable("res");
            tmpFile = File.createTempFile("tmp", ".xml");
            tmpFile.deleteOnExit();
            FlatXmlDataSet.write(partialDataSet, new FileOutputStream(tmpFile));

        } catch (SQLException | DatabaseUnitException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (IDBcon != null) {
                try {
                    IDBcon.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /**
     * データベース復元。
     * @param ds データベース接続情報
     * @throws RuntimeException エラー全般
     */
    public static void Restore(DataSource ds) {
        IDatabaseConnection IDBcon = null;

        try (Connection con = ds.getConnection()) {
            IDBcon = new DatabaseConnection(con);
            //データベース復元
            IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(tmpFile));
            DatabaseOperation.CLEAN_INSERT.execute(IDBcon, dataSet);
        } catch (SQLException | DatabaseUnitException | FileNotFoundException e) {
            throw new RuntimeException(e);
        } finally {
            if (IDBcon != null)
                try {
                    IDBcon.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
        }
    }

    /**
     * テストデータ挿入
     * @param ds           データベース接続情報
     * @param testDataPath テストデータへのパス
     * @throws RuntimeException エラー全般
     */
    public static void InsertTestData(DataSource ds, String testDataPath) {
        IDatabaseConnection IDBcon = null;
        try (Connection con = ds.getConnection()) {
            IDBcon = new DatabaseConnection(con);

            //テストデータ挿入
            IDataSet dataSet;
            dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(testDataPath));
            DatabaseOperation.CLEAN_INSERT.execute(IDBcon, dataSet);
        } catch (SQLException | DatabaseUnitException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (IDBcon != null) {
                try {
                    IDBcon.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /**
     * 現在データベースのレコードを全て削除する。
     * @throws RuntimeException エラー全般
     */
    public static void Delete(DataSource ds) {
        try (Connection con = ds.getConnection();
                Statement stm = con.createStatement()) {
            stm.execute("delete from res");
            stm.execute("delete from thread");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
