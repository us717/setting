package jp.co.us717.func;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import jp.co.us717.exception.GeneralErrorException;

/**
 * private指定にアクセスするための機能。<br>
 * テスト時、外部アクセスを制限しているメソッド、フィールドへのアクセス手段を提供します。
 */
public class FuncPrivateAccessor {

    /**
     * 外部アクセスが禁止されているフィールドの取得。
     * @param    targetObj   対象のフィールドを所持するオブジェクト
     * @param    filedName   変更するフィールド名
     * @return   filed       外部アクセスが禁止されたフィールド
     * @throws GeneralErrorException チェック例外
     */
    static Object getPrivateField(Object targetObj, String filedName) {
        Field filed = null;
        try {
            filed = targetObj.getClass().getDeclaredField(filedName);
        } catch (NoSuchFieldException | SecurityException e) {
            throw new GeneralErrorException(e);
        }
        filed.setAccessible(true);
        return filed;
    }

    /**
     * 外部アクセスが禁止されているフィールドに値を設定します。
     * @param    targetClass    対象のフィールドを所持するオブジェクト
     * @param    filedName      変更するフィールド名
     * @return   filed          アクセスが許可されたフィールド
     * @throws GeneralErrorException チェック例外
     */
    static void SetPrivateField(Object targetObj, String filedName, Object value) {
        Field filed = null;
        try {
            filed = targetObj.getClass().getDeclaredField(filedName);
            filed.setAccessible(true);
            filed.set(targetObj, value);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new GeneralErrorException(e);
        }
    }

    /**
     * 外部アクセスが禁止されているメソッドの実行。
     * 引数あり
     * @param targetClass   対象のメソッドを所持するオブジェクト
     * @param methodName    対象のメソッド名
     * @param dataTypeArray 対象のメソッドの引数の型
     * @param paramArray    対象のメソッドに設定する引数
     * @return
     * @throws GeneralErrorException チェック例外
     */
    static Object doPrivateMethod(Object targetObj, String methodName, Class<?>[] dataTypeArray, Object[] paramArray) {

        Object obj = null;
        try {
            Method method = targetObj.getClass().getDeclaredMethod(methodName, dataTypeArray);
            method.setAccessible(true);
            method.invoke(targetObj, paramArray);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            throw new GeneralErrorException(e);
        }

        return obj;
    }

    /**
    * 外部アクセスが禁止されているメソッドの実行。
    * 引数なし
    * @param targetClass   対象のメソッドを所持するオブジェクト
    * @param methodName    対象のメソッド名
    * @param dataTypeArray 対象のメソッドの引数の型
    * @param paramArray    対象のメソッドに設定する引数
    * @return
    * @throws GeneralErrorException チェック例外
    */
    static Object doPrivateMethod(Object targetClass, String methodName) {

        Object obj = null;
        try {
            Method method = targetClass.getClass().getDeclaredMethod(methodName);
            method.setAccessible(true);
            method.invoke(targetClass, null);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            throw new GeneralErrorException(e);
        }

        return obj;
    }
}
