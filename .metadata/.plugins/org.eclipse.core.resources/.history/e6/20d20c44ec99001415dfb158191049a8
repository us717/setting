package jp.co.us717.func;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.postgresql.ds.PGSimpleDataSource;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * DBテスト用セットアップ。<br>
 * DBテストを行う際にJNDIが使えないため、そのJNDI代替を提供している。
 */
public class FuncJNDISetup {

    /** ログ出力用  */
    private static final Logger log = LogManager.getLogger(FuncDBSetup.class.getName());

    /** データベース接続用情報  */
    private DataSource dataSource;

    /**
     * コンストラクタ。
     */
    public FuncJNDISetup() {
        dataSource = null;
    }

    /**
     * JNDI代替手段を作成する。 <br>
     * テスト時、BeforeClassで呼び出してください。
     */
    public static void SetJNDI() {
        try {
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");
            InitialContext ic = new InitialContext();
            ic.createSubcontext("java:");
            ic.createSubcontext("java:comp");
            ic.createSubcontext("java:comp/env");
            ic.createSubcontext("java:comp/env/jdbc");
            PGSimpleDataSource ds = new PGSimpleDataSource();
            ds.setUser("eas");
            ds.setPassword("sgmw090056P");
            ds.setDatabaseName("paintbbs");
            ds.setServerName("localhost");
            ds.setPortNumber(5432);
            ic.bind("java:comp/env/jdbc/db", ds);
        } catch (NamingException e) {
            log.error("FuncDBSetup:{}", e.getMessage());
        }
    }

    /**
     * データベースの設定を読み込む。<br>
     */
    public void loadSetting() {
        try {
            Context context;
            context = new InitialContext();
            dataSource = (DataSource) context.lookup("java:comp/env/jdbc/db");
        } catch (NamingException e) {
            log.error("Init:{}", e);
        }
    }

    /**
     * DataSourceを取得する。<br>
     * 事前に読み込みメソッドを呼び出しておくこと。
     * @return 読み込んだデータベースの設定
     */
    public DataSource getDataSource() {
        return dataSource;
    }
}
