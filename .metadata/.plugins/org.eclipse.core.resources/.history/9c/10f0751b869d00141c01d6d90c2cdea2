package jp.co.us717.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.us717.func.FuncDBSetup;
import jp.co.us717.func.FuncJNDISetup;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.AfterClass;
import org.mockito.internal.util.reflection.Whitebox;

/**
 * CreateThreadBeanのテスト。
 */
public class HeadLineServletTest {

    /** JNDI */
    private FuncJNDISetup jndi;

    /**
     * JNDIセットアップ、データバックアップ
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理
     */
    @Before
    public void Before() {
        jndi = new FuncJNDISetup();
        jndi.loadSetting();
        FuncDBSetup.Delete(jndi.getDataSource());
        FuncDBSetup.InsertTestData(jndi.getDataSource(), "src/test/resources/sqlTestData/HeadLineServletTest.xml");
    }

    /**
     * データベース復元
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * サーブレットが正常に動作しているか。
     */
    @Test
    public void Get_正常に動作しているか() throws Exception {
        //テスト
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        RequestDispatcher dis = mock(RequestDispatcher.class);
        ServletContext sc = mock(ServletContext.class);

        HeadLineServlet headLine = new HeadLineServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        when(sc.getRequestDispatcher("/WEB-INF/jsp/headkineF.jsp")).thenReturn(dis);
        headLine.init();
        Whitebox.setInternalState(headLine, "dataSource", jndi.getDataSource());
        headLine.doGet(req, res);

        //チェック
        verify(sc, times(1)).getRequestDispatcher(anyString());
        verify(dis, times(1)).forward(req, res);
    }

    /**
     * サーブレットのエラーで例外が返る。<br>
     * dateSourceが設定されていない
     */
    @Test(expected = NullPointerException.class)
    public void Get_処理の失敗() throws Exception {
        //テスト
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        RequestDispatcher dis = mock(RequestDispatcher.class);
        ServletContext sc = mock(ServletContext.class);

        HeadLineServlet headLine = new HeadLineServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        when(sc.getRequestDispatcher("/WEB-INF/jsp/ForwardIndex.jsp")).thenReturn(dis);
        headLine.doGet(req, res);
    }

}