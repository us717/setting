package jp.co.us717.general;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.co.us717.func.FuncDBSetup;
import jp.co.us717.func.FuncJNDISetup;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * データベース接続テスト
 */
public class DBConnectTest {


    /** JNDI */
    private FuncJNDISetup jndi;

    /**
     * JNDIセットアップ、データバックアップ
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource(), "src/test/resources/sqlTestData/HeadLineBeanTest.xml");
    }

    /**
     * テストデータの挿入とバックアップ。
     * @throws Exception エラー全般
     */
    @Before
    public void setup() throws Exception {
        dbSet = new FuncDBSetup();
        dbSet.loadSetting();

        IDatabaseConnection IDBcon = null;
        try (Connection con = dbSet.getDataSource().getConnection();) {
            IDBcon = new DatabaseConnection(con);

            //DBバックアップ
            QueryDataSet partialDataSet = new QueryDataSet(IDBcon);
            partialDataSet.addTable("thread");
            partialDataSet.addTable("res");
            file = File.createTempFile("tmp", ".xml");
            FlatXmlDataSet.write(partialDataSet, new FileOutputStream(file));

            //テストデータ挿入
            IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(
                    "src/test/resources/sqlTestData/DBConnectData.xml"));
            DatabaseOperation.CLEAN_INSERT.execute(IDBcon, dataSet);
        } finally {
            if (IDBcon != null)
                IDBcon.close();
        }
    }

    /**
     * データベース復元。
     * @throws Exception エラー全般
     */
    @After
    public void tearDown() throws Exception {
        IDatabaseConnection IDBcon = null;
        try (Connection con = dbSet.getDataSource().getConnection();) {
            IDBcon = new DatabaseConnection(con);
            //データベース復元
            IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(file));
            DatabaseOperation.CLEAN_INSERT.execute(IDBcon, dataSet);
        } finally {
            if (IDBcon != null)
                IDBcon.close();
        }
    }

    /**
     * 挿入チェック
     * @throws Exception エラー全般
     */
    @Test
    public void test_挿入チェック() throws Exception {
        try (Connection con = dbSet.getDataSource().getConnection();) {
            //挿入データ取得
            PreparedStatement statement = con.prepareStatement("select thread_id from thread");
            ResultSet result = statement.executeQuery();
            result.next();
            assertEquals("3", result.getString("thread_id"));
        }
    }
}
