<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8"/>
<link rel="stylesheet" href="../lib/jquery.simple-color-picker.css"/>
<link rel="stylesheet" href="../lib/jquery-ui.min.css"/>
<script src="../lib/jquery-2.1.1.min.js"></script>
<script src="../lib/jquery.simple-color-picker.js"></script>
<script src="../lib/jquery-ui.min.js"></script>
<script src="../js/paint.js"></script>
<style type="text/css"> canvas { border: 5px solid #999; }
</style>

</head>

<body>
<canvas id="paint" style="background-color:white;" width="400" height="400" >
    このブラウザには対応していません。
</canvas><br>
<button id="undoButton">戻る</button>
<input type="text" id="paintColor" disabled="disabled" size="8"/>
<button id="colorButton">色選択</button><br><br>
<div id="sliderLine" style="width:150px; background:white;"></div>
<div id="sliderAlpha" style="width:150px; background:white;"></div>
<button id="resetButton">削除</button><br><br>

<form action="PostPaint" method="post">
    <button id="postPaintImage" name="postPaintImage">画像作成</button>
</form>

</body>
</html>