$(function(){
    //変数定義
    var ctx, canvas, undo;
    var isClick=false;
    var startX=0.0;
    var startY=0.0;
    var strokeWidth='1';

    //読み込み
    canvas=$('#paint')[0];
    if (!canvas || !canvas.getContext) return;
    ctx=canvas.getContext('2d');

    //初期設定
    ctx.lineJoin="round"; //角を丸くして描画する
    ctx.lineCap="round";
    ctx.fillStyle='white';
    ctx.fillRect(0,0,$('canvas').width(),$('canvas').height());

    //描画動作
    $('#paint').mousedown(function(e){
        isClick=true;
        startX=e.pageX-$(this).offset().left;
        startY=e.pageY-$(this).offset().top;
        undo=ctx.getImageData(0,0,$('canvas').width(),$('canvas').height());
    })
    .mouseup(function() {
        isClick=false;
    })
    .mousemove(function(e){
        var x=e.pageX-$(this).offset().left;
        var y=e.pageY-$(this).offset().top;
        if (!isClick) return;
        ctx.beginPath();
        ctx.moveTo(startX,startY);
        ctx.lineTo(x,y);
        ctx.stroke();
        startX=x;
        startY=y;
    })
    .mouseleave(function(){
        isClick=false;
    })

    //色選択
    $('#colorButton').simpleColorPicker({
        colorsPerLine: 16,
        onChangeColor: function(color) {$('#paintColor').val(color).css("background-color",color);
                                            ctx.strokeStyle=color;}
    })

    //線の大きさ
    $('#sliderLine').slider({
        min: 1,
        max: 50,
        step: 1,
        value: 1,
        range: "min",
        change:function(e,ui) {
           ctx.lineWidth=ui.value;
        }
    })

    //透過度
    $('#sliderAlpha').slider({
        min: 0.0,
        max: 1.0,
        step: 0.01,
        value: 1.0,
        range:'min',
        change:function(e,ui) {
            ctx.globalAlpha=ui.value;
        }
    })

    //アンドゥ
    $('#undoButton').click(function(){
        ctx.putImageData(undo,0,0);
    })

    //削除
    $('#resetButton').click(function(e){
        ctx.fillRect(0,0,$('canvas').width(),$('canvas').height());//透明化するので削除ではなく白く塗りつぶしている
    })

    //画像作成送信
    $('#postPaintImage').click(function(){
        var postImage=canvas.toDataURL('image/jpeg');
        postImage=postImage.replace('data:image/jpeg;base64,','');
        $('#postPaintImage')[0].value=postImage;
    })
});
