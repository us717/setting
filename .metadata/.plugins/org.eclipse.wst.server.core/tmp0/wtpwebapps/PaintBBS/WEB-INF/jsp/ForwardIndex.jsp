<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8" />
<title>PaintxBS</title>
</head>
<body>
 <p>テスト結果:${requestScope.data[1]}</p>
 <p>テスト結果:${requestScope.data.size()}</p>
 <c:forEach var="obj" items="${requestScope.data}">
  <c:out value="${obj}" />
  <img src="${obj}">
 </c:forEach>

</body>
</html>